# http://adventofcode.com/day/4
"""
--- Day 5: Doesn't He Have Intern-Elves For This? ---

Santa needs help figuring out which strings in his text file are naughty or nice.

A nice string is one with all of the following properties:

It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
"""

import argparse
import logging
import os

import classes.naughtornice as naughtynice

__author__ = 'jedmitten@alumni.cmu.edu'


PART = 2  # defines the part of the advent problem


def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-i', '--input-str', action='store', help='The string to check for naughty/nice')
    group.add_argument('-f', '--input-file', action='store', help='File of input strings to check')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    parsers = []
    naughty_words = []
    nice_words = []
    if opts.input_str:
        parsers.append(naughtynice.NaughtyOrNiceString(opts.input_str))
    else:
        with open(opts.input_file, 'r') as f:
            lines = [''.join(line.split()) for line in f.readlines()]
        for line in lines:
            non_parser = naughtynice.NaughtyOrNiceString(line)
            if PART == 1:
                non_parser.parse_part1()
                if non_parser.is_nice_part_1():
                    nice_words.append(non_parser.content)
                else:
                    naughty_words.append(non_parser.content)
            else:
                non_parser.parse_part2()
                if non_parser.is_nice_part_2():
                    nice_words.append(non_parser.content)
                else:
                    naughty_words.append(non_parser.content)
    print('Naughty words: {}'.format(naughty_words))
    print('Nice words: {}'.format(nice_words))
    print('There were {} {} strings'.format(len(nice_words), naughtynice.NaughtyOrNiceString.STR_NICE))

if __name__ == '__main__':
    main(get_opts())