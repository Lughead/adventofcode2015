# http://adventofcode.com/day/2
"""
--- Day 3: Perfectly Spherical Houses in a Vacuum ---

Santa is delivering presents to an infinite two-dimensional grid of houses.

He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west (<). After each move, he delivers another present to the house at his new location.

However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and Santa ends up visiting some houses more than once. How many houses receive at least one present?
"""

import argparse
import fileinput
import logging
import os

import classes.boxes as boxes

__author__ = 'jedmitten@alumni.cmu.edu'


def setup_logging(verbose=False):
    log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', action='store', help='A file containing rectangle dimensions ("LxHxW")')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def calc_wrapping_paper(box):
    return box.surface_area() + box.smallest_side()


def calc_ribbon_length(box):
    return box.smallest_perimeter() + box.volume()


def main(opts):
    log_name = setup_logging()
    log = logging.getLogger(log_name)

    all_dims = []
    for line in fileinput.input():
        all_dims.append(line)
    all_boxes = []
    for spec in all_dims:
        dims = spec.lower().split('x')
        all_boxes.append(boxes.Box(l = int(dims[0]), h = int(dims[1]), w = int(dims[2])))
    log.info('There are {} boxes created'.format(len(all_boxes)))
    total_paper_area = 0
    total_ribbon_length = 0
    for idx in range(len(all_boxes)):
        log.debug('###[ BOX {:02d} ]###'.format(idx))
        log.debug('dims: {}x{}x{}'.format(all_boxes[idx].l, all_boxes[idx].h, all_boxes[idx].w))
        log.debug('surface area: {}'.format(all_boxes[idx].surface_area()))
        log.debug('sides: {}'.format(all_boxes[idx].sides))
        log.debug('smallest side: {}'.format(all_boxes[idx].smallest_side()))
        log.debug('smallest perimeter: {}'.format(all_boxes[idx].smallest_perimeter()))
        log.debug('volume: {}'.format(all_boxes[idx].volume()))
        log.debug('wrapping paper area: {}'.format(calc_wrapping_paper(all_boxes[idx])))
        log.debug('ribbon length: {}'.format(calc_ribbon_length(all_boxes[idx])))
        total_paper_area += calc_wrapping_paper(all_boxes[idx])
        total_ribbon_length += calc_ribbon_length(all_boxes[idx])
    print('Total wrapping paper area required: {}'.format(total_paper_area))
    print('Total ribbon length required: {}'.format(total_ribbon_length))

    
if __name__ == '__main__':
    main(get_opts())