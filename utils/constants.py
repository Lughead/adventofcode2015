import logging

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)

VOWELS = ('a', 'e', 'i', 'o', 'u')
STR_NUMBERS = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
NUMBERS = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
MAX_16BIT = 2**16
