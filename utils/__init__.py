import logging

import constants

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)

def has_three_vowels(in_str):
    str_len = len(in_str)
    vowels = []
    for c in in_str:
        if c in constants.VOWELS:
            vowels.append(c)
        if len(vowels) > 2:
            return True
    return False


def is_int(v):
    """
    Returns the truth of a value being an int
    :param v: An integer or other
    :return: boolean
    """
    return isinstance(v, int)


def is_int_str(v):
    """
    Returns the truth of a string being an int representation
    :param v: A string
    :return: boolean
    """
    for c in v:
        if c not in constants.STR_NUMBERS:
            return False
    return True


def is_16bit_int(v):
    if is_int(v):
        if 0 <= v < 2**16:
            return True
    return False
