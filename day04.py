# http://adventofcode.com/day/4
"""
--- Day 4: The Ideal Stocking Stuffer ---

Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all the economically forward-thinking little girls and boys.

To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five zeroes. The input to the MD5 hash is some secret key (your puzzle input, given below) followed by a number in decimal. To mine AdventCoins, you must find Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash.
"""

import argparse
import hashlib
import logging
import os

__author__ = 'jedmitten@alumni.cmu.edu'



def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--key', required=True, action='store', help='A prefix to append to numbers')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    log_name = setup_logging(opts.verbose or False)
    log = logging.getLogger(log_name)

    prefix = opts.key
    log.debug('prefix: {}'.format(prefix))

    PART = 2 # defines the part of the advent problem
    if PART == 1:
        HASH_PREFIX = '00000'
    elif PART == 2:
        HASH_PREFIX = '000000'

    winner = -1
    i = 0
    while True:
        m = hashlib.md5()
        input = '{}{}'.format(prefix, i)
        m.update(input)
        md5sum = m.hexdigest()
        # log.debug('input: {}, md5sum: {}'.format(input, md5sum))
        if md5sum.startswith(HASH_PREFIX):
            break
        i += 1
        if not i % 10000:
            log.debug(i)
    print('The first winning value is {}{}'.format(prefix, i))



if __name__ == '__main__':
    main(get_opts())