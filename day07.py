# http://adventofcode.com/day/7
"""
--- Day 7: Some Assembly Required ---

This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates! Unfortunately, little Bobby is a little under the recommended age range, and he needs help assembling the circuit.

Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535). A signal is provided to each wire by a gate, another wire, or some specific value. Each wire can only get a signal from one source, but can provide its signal to multiple destinations. A gate provides no signal until all of its inputs have a signal.

The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y to an AND gate, and then connect its output to wire z.
"""

import argparse
import logging
import os
from pprint import pprint

import classes.gates as gates

__author__ = 'jedmitten@alumni.cmu.edu'


PART = 2  # defines the part of the advent problem


def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--input-file', action='store', help='File of input strings to check')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    log_name = setup_logging(opts.verbose or False)
    log = logging.getLogger(log_name)

    gate_parser = gates.CommandParser()
    log.info('Reading input file: [{}]'.format(opts.input_file))
    with open(opts.input_file, 'r') as f:
        commands = [f.rstrip() for f in f.readlines() if f]

    d_wires = {}
    l_gates = []

    if PART == 2:
        idx = commands.index('14146 -> b')
        commands[idx] = '956 -> b'  # override per instructions

    for command in commands:
        gate_parser.parse(command, d_wires, l_gates)

    # for name in d_wires:
    #     print('{}: {}'.format(name, d_wires[name].get_signal()))

    print('{}: {}'.format('a', d_wires['a'].get_signal()))


if __name__ == '__main__':
    main(get_opts())
