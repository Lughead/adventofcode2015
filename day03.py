# http://adventofcode.com/day/2
"""
--- Day 3: Perfectly Spherical Houses in a Vacuum ---

Santa is delivering presents to an infinite two-dimensional grid of houses.

He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west (<). After each move, he delivers another present to the house at his new location.

However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and Santa ends up visiting some houses more than once. How many houses receive at least one present?
"""

import argparse
import logging
import os

import classes.directions as directions

__author__ = 'jedmitten@alumni.cmu.edu'


def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True, action='store', help='A file containing rectangle dimensions ("LxHxW")')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    log_name = setup_logging(opts.verbose or False)
    log = logging.getLogger(log_name)

    with open(opts.input, 'r') as f:
        all_directions = [''.join(f.split()) for f in f.readlines() if ''.join(f.split())][0]

    start = (0, 0)
    santa = directions.Santa(starting_coords=start)
    robot = directions.Santa(starting_coords=start)
    grid = directions.HousingGrid()

    PART = 2  # defines the part of the advent problem
    if PART == 1:
        grid.houses[start] = 1  # visited by Santa
        for d in all_directions:
            santa.move_to(d, grid)
    if PART == 2:
        grid.houses[start] = 2  # visited by both Santa and Robo-Santa
        for i in range(len(all_directions)):
            if i % 2:
                robot.move_to(all_directions[i], grid)
                continue
            santa.move_to(all_directions[i], grid)

    print('A total of {} houses were visited. Each got at least one present'.format(len(grid.houses)))

if __name__ == '__main__':
    main(get_opts())