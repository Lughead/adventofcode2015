# http://adventofcode.com/day/4
"""
--- Day 6: Probably a Fire Hazard ---

Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.
"""

import argparse
import logging
import os

import classes.lights as lights

__author__ = 'jedmitten@alumni.cmu.edu'


PART = 2  # defines the part of the advent problem


def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--input-file', action='store', required=True, help='File of input strings to check')
    parser.add_argument('--dimensions', action='store', required=True, help='The dimensions of the grid')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    log_name = setup_logging(opts.verbose or False)
    log = logging.getLogger(log_name)

    lights_parser = lights.InstructionParser()
    log.info('Reading input file: [{}]'.format(opts.input_file))
    with open(opts.input_file, 'r') as f:
        commands = [f.rstrip() for f in f.readlines() if f]
    if PART == 1:
        grid = lights.LightGrid_Part1(int(opts.dimensions))
    else:
        grid = lights.LightGrid_Part2(int(opts.dimensions))

    for command in commands:
        lights_parser.parse_light_instruction(command, grid)

    if PART == 1:
        print('{} lights are lit'.format(len(grid.on_cells)))
    else:
        print('The total brigthness is: {}'.format(grid.total_brightness))

if __name__ == '__main__':
    main(get_opts())
