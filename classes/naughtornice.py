import logging

import utils.constants as constants

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class NaughtyOrNiceString:
    NAUGHTY_STRINGS = ['ab', 'cd', 'pq', 'xy']
    STR_NAUGHTY = 'NAUGHTY'
    STR_NICE = 'NICE'

    def __init__(self, content):
        self.content = content
        self.content_len = len(content)
        # for part 1
        self.has_double_letter = False
        self.has_naughty_strs = False
        self.vowels = []
        # for part 2
        self.has_valid_str_pairs = False
        self.has_str_threesome = False
        self.str_pairs = {}  # (x, y): [start_index]

    @property
    def has_three_vowels(self):
        return len(self.vowels) > 2

    def is_nice_part_1(self):
        if self.has_three_vowels and self.has_double_letter:
            if not self.has_naughty_strs:
                return True
        return False

    def is_nice_part_2(self):
        if self.has_str_threesome and self.has_valid_str_pairs:
            return True
        return False

    def _create_string_pairs(self):
        for i in range(self.content_len - 1):  # cut off last index because we're creating pairs
            key_str = self.content[i:i + 2]
            self.str_pairs.setdefault(key_str, []).append(i)

    def _check_valid_str_pairs(self):
        if not self.str_pairs:
            return
        for k_str, l_indices in self.str_pairs.items():
            if len(l_indices) < 2:
                continue
            for i in range(len(l_indices) - 1):
                cur_idx = l_indices[i]
                remaining_idxs = l_indices[i + 1:]
                for j in range(len(remaining_idxs)):
                    if remaining_idxs[j] - cur_idx > 1:
                        self.has_valid_str_pairs = True
                        return

    def _check_str_threesome(self):
        for i in range(self.content_len - 2):  # cut off last 2 indices because we're checking 3s
            triple = self.content[i:i + 3]
            if triple[0] == triple[2]:
                self.has_str_threesome = True
                return

    def parse_part2(self):
        self._create_string_pairs()
        self._check_valid_str_pairs()
        self._check_str_threesome()

    def parse_part1(self):
        for cur_i in range(self.content_len):
            prev_i = cur_i - 1
            if prev_i >= 0:
                if not self.has_double_letter and (self.content[prev_i] == self.content[cur_i]):
                    self.has_double_letter = True
                    log.info('Found a double letter [{}] at index {}'.format(self.content[cur_i], cur_i))
                if not self.has_naughty_strs and (self.content[prev_i] + self.content[cur_i] in self.NAUGHTY_STRINGS):
                    self.has_naughty_strs = True
                    log.info('Found a naughty string [{}] at index {}'.format(
                            self.content[prev_i] + self.content[cur_i], cur_i))
            if not self.has_three_vowels and (self.content[cur_i] in constants.VOWELS):
                self.vowels.append(self.content[cur_i])
                log.info('Adding vowel [{}] to self.vowels'.format(self.content[cur_i]))

    def __str__(self):
        if self.is_nice_part_1():
            return self.STR_NICE
        return self.STR_NAUGHTY