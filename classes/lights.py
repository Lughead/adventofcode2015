import logging
import utils.constants as constants

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class InvalidCommandError(Exception):
    pass


class GridIsNoneError(Exception):
    pass


class LightGrid_Part2(object):
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.cells = []
        for i in range(self.dimensions):
            row = [0 for idx in range(self.dimensions)]
            self.cells.append(row)
        self.total_brightness = 0

    def light(self, x, y):
        self.cells[x][y] += 1
        self.total_brightness += 1

    def extinguish(self, x, y):
        if self.cells[x][y] - 1 >= 0:
            self.cells[x][y] -= 1
            self.total_brightness -= 1

    def toggle(self, x, y):
        self.cells[x][y] += 2
        self.total_brightness += 2

    def __getitem__(self, item):
        return self.cells.__getitem__(item)

    def __len__(self):
        return self.cells.__len__()


class LightGrid_Part1(object):
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.cells = []
        for i in range(self.dimensions):
            row = [False for idx in range(self.dimensions)]
            self.cells.append(row)

    def light(self, x, y):
        self.cells[x][y] = True

    def extinguish(self, x, y):
        self.cells[x][y] = False

    def toggle(self, x, y):
        self.cells[x][y] = not self.cells[x][y]

    @property
    def on_cells(self):
        on_cells = []
        for i in range(self.dimensions):
            for j in range(self.dimensions):
                if self.cells[i][j]:
                    on_cells.append((i, j))
        return on_cells

    @property
    def off_cells(self):
        off_cells = []
        for i in range(self.dimensions):
            for j in range(self.dimensions):
                if not self.cells[i][j]:
                    off_cells.append((i, j))
        return off_cells

    def __getitem__(self, item):
        return self.cells.__getitem__(item)

    def __len__(self):
        return self.cells.__len__()

    def show_cells(self):
        ret_str = ''
        ret_str += '-' * self.dimensions
        for i in range(self.dimensions):
            s = ''
            for j in range(self.dimensions):
                if self.cells[i][j]:
                    s += 'O'
                else:
                    s += '.'
            ret_str += '{}\n'.format(s)
        ret_str += '-' * self.dimensions
        return ret_str

class InstructionParser:
    SEP = ' through '  # all valid commands have this between coordinates
    INST = {'turn on': 0,
            'turn off': 1,
            'toggle': 2}

    def __init__(self):
        self.instruction = -1
        self.starting_coord = (-1, -1)
        self.ending_coord = (-1, -1)

    def parse_light_instruction(self, command, grid):
        """
        :param command: A string representation of a command (e.g., 'toggle 0,0 through 999,999')
        :return: A tuple of (command, first_coord, last_coord)
        """
        log.debug('Received command: [{}]'.format(command))
        first_num = 0
        for i in range(len(command)):
            if command[i] in constants.STR_NUMBERS:
                first_num = i
                break
        if not first_num:
            raise InvalidCommandError()
        com_phrase = command[:first_num - 1]  # remove trailing space
        str_coords = command[first_num:]
        l_coords = str_coords.split(self.SEP)  # will always be len 2
        self.instruction = self.INST[com_phrase.lower()]
        self.starting_coord = [int(c) for c in l_coords[0].split(',')]
        self.ending_coord = [int(c) for c in l_coords[1].split(',')]
        log.debug('Parsed command phrase: [{}] and coordinates: ({}, {})'.format(com_phrase, self.starting_coord,
                                                                                 self.ending_coord))
        self._execute_instructions(grid)

    def _execute_instructions(self, grid):
        """ assuming grid is 1000x1000 """
        if grid is None:
            raise GridIsNoneError()
        x_start = self.starting_coord[0]
        x_end = self.ending_coord[0]
        y_start = self.starting_coord[1]
        y_end = self.ending_coord[1]
        x_cur = x_start  # convenience variable. could use x_start throughout
        while x_cur <= x_end:
            y_cur = y_start
            while y_cur <= y_end:
                if self.instruction == self.INST['turn on']:
                    grid.light(x_cur, y_cur)
                elif self.instruction == self.INST['turn off']:
                    grid.extinguish(x_cur, y_cur)
                elif self.instruction == self.INST['toggle']:
                    grid.toggle(x_cur, y_cur)
                y_cur += 1
            x_cur += 1
