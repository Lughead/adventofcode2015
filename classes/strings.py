import logging

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class UnescapedString:
    CONV = {'"': r'\"',
            '\\': '\\\\'}

    def __init__(self, literal):
        self.literal = literal

    @property
    def escaped_value_part1(self):
        return self.literal[1:len(self.literal) - 1].decode('string-escape')

    @property
    def escaped_value_part2(self):
        ret_str = ''
        for c in self.literal:
            if c in self.CONV:
                ret_str += self.CONV[c]
            else:
                ret_str += c
        ret_str = '"{}"'.format(ret_str)
        return ret_str


class CharacterParser:
    def __init__(self):
        pass

    @classmethod
    def parse(cls, escaped_string):
        return UnescapedString(literal=escaped_string)