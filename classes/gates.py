import logging
import utils
import utils.constants as constants

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class InvalidGateInput(Exception):
    pass


class InvalidInstruction(Exception):
    pass


class Wire:
    def __init__(self, name='', signal=None):
        self.name = name
        self.signal = signal  # either a Gate or a number

    def get_signal(self):
        if utils.is_int(self.signal):
            return self.signal
        # signal is a Gate
        self.signal = self.signal.calc_output()
        return self.get_signal()


class Gate:
    def __init__(self, input1, input2, operator):
        self.input1 = input1
        self.input2 = input2
        if not isinstance(self.input1, Wire) or not isinstance(self.input2, Wire):
            raise InvalidGateInput()
        self.operator = operator

    def calc_output(self):
        if self.operator == 'not':
            val = ~self.input1.get_signal()
            if val < 0:
                return constants.MAX_16BIT + val
            return val
        elif self.operator == 'and':
            return self.input1.get_signal() & self.input2.get_signal()
        elif self.operator == 'or':
            return self.input1.get_signal() | self.input2.get_signal()
        elif self.operator == 'lshift':
            return self.input1.get_signal() << self.input2.get_signal()
        elif self.operator == 'rshift':
            return self.input1.get_signal() >> self.input2.get_signal()


class CommandParser:
    SEP = '->'

    def __init__(self):
        pass

    @classmethod
    def parse(cls, cmd, wires, gates):
        cmd = cmd.lower()
        parts = cmd.split()
        if cls.SEP not in parts:
            raise InvalidInstruction()
        lhs, destination = cmd.split(' ' + cls.SEP + ' ')
        lhs_parts = lhs.split()
        if len(lhs_parts) == 1:  # signal -> destination, use AND gate as input to Wire
            wire = wires.setdefault(destination, Wire(name=destination))
            if utils.is_int_str(lhs):
                signal_wire_1 = Wire(name=lhs, signal=int(lhs))
                signal_wire_2 = signal_wire_1
            else:
                signal_wire_1 = wires.setdefault(lhs, Wire(name=lhs))
                signal_wire_2 = signal_wire_1
            gate = Gate(input1=signal_wire_1, input2=signal_wire_2, operator='and')
            wire.signal = gate
        elif lhs.startswith('not'):  # NOT source -> destination
            op, source = lhs.split()
            wire = wires.setdefault(destination, Wire(name=destination))
            in_wire = wires.setdefault(source, Wire(name=source))
            if utils.is_int_str(source):
                in_wire.signal = int(source)
            dummy_wire = Wire(name='dummy', signal=-1)
            gate = Gate(input1=in_wire, input2=dummy_wire, operator=op)
            wire.signal = gate
        elif len(lhs.split()) == 3:  # in1 op in2 -> destination
            parts = lhs.split()
            wire_name1 = parts[0]
            wire_name2 = parts[2]
            op = parts[1]
            wire = wires.setdefault(destination, Wire(name=destination))
            if utils.is_int_str(wire_name1):
                in_wire_1 = Wire(name=wire_name1, signal=int(wire_name1))
            else:
                in_wire_1 = wires.setdefault(wire_name1, Wire(name=wire_name1))
            if utils.is_int_str(wire_name2):
                in_wire_2 = Wire(name=wire_name2, signal=int(wire_name2))
            else:
                in_wire_2 = wires.setdefault(wire_name2, Wire(name=wire_name2))
            gate = Gate(input1=in_wire_1, input2=in_wire_2, operator=op)
            wire.signal = gate
        else:
            raise InvalidInstruction()
        wires[destination] = wire
        if gate:
            gates.append(gate)