import logging

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class Box:
    def __init__(self, l, h, w):
        self.l = l
        self.h = h
        self.w = w
        
        self.sides = []
        self._calculate_sides()
        
    def _calculate_sides(self):
        self.sides.append(self.l * self.w)
        self.sides.append(self.w * self.h)
        self.sides.append(self.h * self.l)
        log.debug('sides: {}'.format(self.sides))

    def surface_area(self):
        return sum(self.sides) * 2
        
    def smallest_side(self):
        return min(self.sides)

    def get_dims(self):
        return 'x'.join([self.l, self.h, self.w])

    def volume(self):
        return self.l * self.h * self.w

    def smallest_perimeter(self):
        # find two smallest dimensions and multiply by 2
        dims = sorted([self.l, self.h, self.w])
        while len(dims) > 2:
            dims.pop()
        log.debug('two smallest dims: {}'.format(dims))
        return sum(dims) * 2
