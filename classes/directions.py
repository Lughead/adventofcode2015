import collections
import logging

__author__ = 'jedmitten@alumni.cmu.edu'

log = logging.getLogger(__name__)


class NotAHouseError(Exception):
    pass


class InvalidDirectionError(Exception):
    pass


class InvalidCoordinatesError(Exception):
    pass


DirCmd = collections.namedtuple('DirCmd', ['command', 'axis_mod'])
NORTH = DirCmd(command='^', axis_mod=(0, 1))
SOUTH = DirCmd(command='v', axis_mod=(0, -1))
EAST = DirCmd(command='>', axis_mod=(1, 0))
WEST = DirCmd(command='<', axis_mod=(-1, 0))
DIRECTIONS = {'SOUTH': SOUTH,
              'NORTH': NORTH,
              'EAST': EAST,
              'WEST': WEST
              }
valid_commands = []
for direction, t in DIRECTIONS.items():
    valid_commands.append(t.command)


class HousingGrid(object):
    def __init__(self):
        self.houses = {(0, 0): 0}


class Santa(object):
    def __init__(self, starting_coords):
        # keys will be tuples, values will be int
        self.cur_coords = starting_coords
        self.houses_visited = set([starting_coords])

    def move_to(self, direction, grid):
        if direction not in valid_commands:
            log.error('{} was not found in {}'.format(direction, valid_commands))
            raise InvalidDirectionError('The following is an invalid direction: [{}]'.format(direction))
        log_dir = ''
        x = self.cur_coords[0]
        y = self.cur_coords[1]
        for d, v in DIRECTIONS.items():
            if direction == v.command:
                log_dir = d
                x += v.axis_mod[0]
                y += v.axis_mod[1]
                break
        # update the grid presents value
        p = grid.houses.setdefault((x, y), 0)
        grid.houses[(x, y)] = (p + 1)
        self.cur_coords = (x, y)
        self.houses_visited.add((x, y))

        log.debug('Santa has gone {}. He\'s now at {}'.format(log_dir, self.cur_coords))
