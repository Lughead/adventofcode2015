import sys


CMD = {'up': '(',
       'down': ')'
      }

class UpDownParser:
    def __init__(self, cmds):
        self.commands = cmds
        self.first_basement_flag = False
        
    @property
    def ups(self):
        return self.commands.count(CMD['up'])
        
    @property
    def downs(self):
        return self.commands.count(CMD['down'])

    def iterate(self):
        floor = 0
        first_basement_pos = 0
        c_count = len(self.commands)
        for pos in range(c_count):
            c = self.commands[pos]
            if c == CMD['up']:
                floor += 1
                print('Santa went up!')
            if c == CMD['down']:
                floor -= 1
                print('Santa went down!')
            if floor == -1:
                if not first_basement_pos:
                    first_basement_pos = pos
        print('First entrance into basement is at position: {}'.format(first_basement_pos + 1))
        print('Santa ended on floor {}'.format(floor))
        

def main():
    commands = sys.argv[1]
    # print('Got commands [{}]'.format(commands))
    p = UpDownParser(commands)
    p.iterate()
    
    
    
if __name__ == '__main__':
    main()