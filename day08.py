# http://adventofcode.com/day/8
"""
--- Day 8: Matchsticks ---

Space on the sleigh is limited this year, and so Santa will be bringing his list as a digital copy. He needs to know how much space it will take up when stored.

It is common in many programming languages to provide a way to escape special characters in strings. For example, C, JavaScript, Perl, Python, and even PHP handle special characters in very similar ways.

However, it is important to realize the difference between the number of characters in the code representation of the string literal and the number of characters in the in-memory string itself.
"""

import argparse
import logging
import os

import classes.strings as strings

__author__ = 'jedmitten@alumni.cmu.edu'


PART = 1  # defines the part of the advent problem


def setup_logging(verbose=False):
    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    format_str = '%(asctime)-15s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=log_level, format=format_str)
    file_name = os.path.split(os.path.realpath(__file__))[1]
    log_name = os.path.splitext(file_name)[0]
    return log_name


def get_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--input-file', required=True, action='store', help='File of input strings to check')
    parser.add_argument('-v', '--verbose', action='store_true', help='Display debugging output')
    return parser.parse_args()


def main(opts):
    log_name = setup_logging(opts.verbose or False)
    log = logging.getLogger(log_name)

    c_parser = strings.CharacterParser()
    with open(opts.input_file, 'r') as f:
        raw_strings = [f.rstrip() for f in f.readlines() if f]

    escaped_strings = []
    literal_characters = 0
    memory_characters = 0
    expanded_characters = 0
    for s in raw_strings:
        log.info('Parsing [{}]'.format(s))
        es = c_parser.parse(s)
        literal_characters += len(es.literal)
        part1_escaped = es.escaped_value_part1
        part2_escaped = es.escaped_value_part2
        memory_characters += len(part1_escaped)
        expanded_characters += len(part2_escaped)
        escaped_strings.append(es)
        log.debug('{} (len: {}) -> {} (len: {}) / {} (len: {})'.format(es.literal, len(es.literal),
                                                                       part1_escaped, len(part1_escaped),
                                                                       part2_escaped, len(part2_escaped)))

    print('String Literal characters: {}'.format(literal_characters))
    print('In-Memory Characters: {}'.format(memory_characters))
    print('Answer (Part 1): {}'.format(literal_characters - memory_characters))
    print('Answer (Part 2): {}'.format(expanded_characters - literal_characters))

if __name__ == '__main__':
    main(get_opts())
